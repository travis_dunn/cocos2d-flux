var FluxAction = cc.Class.extend({
    type: null,
    payload: null,
    meta: null,
    error: null,
    ctor:function (type, payload, error, meta) {
        cc.assert(type, "Attempted creating FluxAction with no type string");

        this.type = type;
        this.payload = payload;
        this.error = error ? true : false;
        this.meta = meta;

        if (this.error && payload && (typeof payload !== "Error")) {
            cc.warn("Payloads for a FluxAction with an error SHOULD be typeof Error");
        };
    }
})
