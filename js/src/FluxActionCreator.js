var FluxActionCreator = cc.Class.extend({
    payload: null,
    ctor:function (payload) {
        this.payload = payload;
    },
    create:function () {
        cc.error("Attempted calling create on FluxActionCreator subclass without implementing the create method");
        throw "Method not implemented";
    }
})
