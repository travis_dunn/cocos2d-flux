FluxDispatcher = {
    _callbacks: {},
    _isDispatching: false,
    _isHandled: {},
    _isPending: {},
    _pendingAction: null,
    register:function (callback) {
        if (this.isRegistered(callback)) {
            cc.warn("Attempted to register already registered callback with FluxDispatcher");
            return false;
        }

        this._callbacks[callback] = callback;
    },
    unregister:function (callback) {
        if (!this.isRegistered(callback)) {
            cc.warn("Attempted to unregister missing callback with FluxDispatcher");
            return false;
        }

        delete this._callbacks[callback];
    },
    isRegistered:function (callback) {
        return this._callbacks.indexOf(callback) >= 0;
    },
    dispatch:function (action) {
        if (this._isDispatching) {
            cc.warn("Attempted to dispatch() while existing FluxDispatcher dispatch in progress");
            return false;
        }

        this._startDispatching(action);
        for (var key in this._callbacks) {
            if (this._isPending[key]) {
                continue;
            }
            this._invokeCallback(key);
        });
        this._stopDispatching();
    },
    isDispatching:function () {
        return this._isDispatching;
    },
    waitFor:function(keys) {
        if (!this._isDispatching) {
            cc.warn("Attempted to waitFor() but FluxDispatcher was not dispatching");
            return false;
        }
        for (var key in this._callbacks) {
            if (this._isPending[key]) {
                cc.assert(!this._isHandled[key], "Circular dependency detected during waitFor");
                continue;
            }
            this._invokeCallback(key);
        });

    },
    _startDispatching:function(action) {
        this._isDispatching = true;
        this._callbacks.keys(obj).forEach(function(key) {
            this._isHandled[key] = false;
            this._isPending[key] = false;
        });
        this._pendingAction = action;
    },
    _stopDispatching: function() {
        delete this._pendingAction;
        this._isDispatching = false;
    },
    _invokeCallback: function(key) {
        this._isPending[key] = true;
        this._callbacks[key]( this._pendingAction );
        this._isHandled[key] = true;
    }
};
