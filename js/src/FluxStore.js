var _fluxStore = cc.Class.extend({
    _state: null,
    _listeners: null,
    ctor:function () {
        this._state = null;
        this._listeners = {};
        FluxDispatcher.register(this.onDispatch);
    },
    getState:function () {
        return this._state;
    },
    onDispatch:function (action) {
        cc.error("Attempted calling onDispatch on FluxStore subclass without implementing the onDispatch method");
        throw "Method not implemented";
    },
    addEventListener: function(callback) {
        if (this.isListening(callback)) {
            cc.warn("Callback attempted to listen to FluxStore when already listening");
            return false;
        }

        var listener = cc.EventListener.create({
            event: cc.EventListener.CUSTOM,
            eventName: this._eventName(),
            callback: callback
        });
        this._listeners[callback] = listener;
        return listener;
    },
    removeEventListener: function(callback) {
        if (!this.isListening(callback)) {
            cc.warn("Callback attempted to stop listening to FluxStore but was not listening");
            return false;
        }
        cc.eventManager.removeListener( this._listeners[callback] );
        delete this._listeners[callback];
    },
    isListening:function (callback) {
        return this._listeners.hasOwnProperty(callback);
    },
    _emitChange:function () {
        var changed = new cc.EventCustom( this._eventName() );
        cc.eventManager.dispatchEvent(changed);
    },
    _eventName:function() {
        return "store-change-event-"+this.__instanceId;
    }
});

var FluxStore = {
    create: function(obj) {
        var store = new _fluxStore();
        if (obj.hasOwnProperty("state")) {
            store._state = obj.state;
        }
        if (obj.hasOwnProperty("onDispatch")) {
            store.onDispatch = obj.onDispatch;
        }

        return store;
    }
}
