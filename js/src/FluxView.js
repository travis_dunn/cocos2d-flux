var FluxView = cc.Layer.extend({
    ctor:function () {
        this._super();
    },
    registerStore: function(store, callback) {
        store.addEventListener(function(event) {
            var data = event.getUserData();
            callback(data);
        });

    }
})
