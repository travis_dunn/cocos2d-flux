var GameSettingsData = {
    storeSettings: function(settings, callback) {
        cc.sys.localStorage.setItem("GAME_SETTINGS", settings);
        callback();
    },
    loadSettings: function(callback) {
        var settings = cc.sys.localStorage.getItem("GAME_SETTINGS");
        callback(settings);
    }
}
