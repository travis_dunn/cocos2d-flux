var GameHudViewLayer = FluxView.extend({
    _pointsLabel: null,
    ctor:function () {
        this._super();
        this.init();
    },
    init:function () {
        this.initLabels();

        PlayerStore.addEventListener(this.onPlayerUpdated);
    },
    initLabels: function() {
        this._pointsLabel = new cc.LabelTTF("Points: 0");

        this.addChild(this._pointsLabel);
    },
    onPlayerUpdated:function (data) {
        if (data.delta.points) {
            var player = PlayerStore.getState();
            this.modifyLabel()
        }
    }
    modifyLabel:function() {
    }
});
