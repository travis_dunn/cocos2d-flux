var GameBoardViewLayer = FluxView.extend({
    ctor:function () {
        this._super();
        this.init();
    },
    init:function () {
        DealerStore.addEventListener(this.onTilesDealt);
        MatchStore.addEventListener(this.onMatchUpdated);
        ScoringStore.addEventListener(this.onMatchScored);
    },
    onTilesDealt:function(data) {

    },
    onMatchUpdated:function(data) {

    },
    onMatchScored:function (data) {
        if ( GameBoardStore.isMissingTiles() ) {
            var tiles = GameBoardStore.getState().tiles;
            var ac = new ReplaceTilesActionCreator(tiles);
            ac.create();
        }
    },
});
