var MatchViewLayer = FluxView.extend({
    _selectionPath: null,
    ctor:function () {
        this._super();
        this.init();

    },
    init:function () {
        this._selectionPath = new cc.DrawNode();
        this.addChild(this._selectionPath);

        if (cc.sys.capabilities.mouse) {
            var mouseListener = cc.eventManager.addListener({
                event: cc.EventListener.MOUSE,
                onMouseDown: function(e) { e.getCurrentTarget().onSelectStart(e) },
                onMouseMove: function(e) { e.getCurrentTarget().onSelectMove(e) },
                onMouseUp: function(e) { e.getCurrentTarget().onSelectEnd(e) }
            }, this);
        } else if (cc.sys.capabilities.touches) {
            var touchListener = cc.eventManager.addListener({
                event: cc.EventListener.TOUCH_ALL_AT_ONCE,
                onTouchBegan: function(e) { e.getCurrentTarget().onSelectStart(e.getTouches()[0]) },
                onTouchesMoved: function(e) { e.getCurrentTarget().onSelectMove(e.getTouches()[0]) },
                onTouchEnded: function(e) { e.getCurrentTarget().onSelectEnd(e.getTouches()[0]) }
            }, this);
        }

        MatchStore.addEventListener(this.onMatchUpdated);
        ScoringStore.addEventListener(this.onMatchScored);
    },
    _drawSelectionPath:function() {
        this._selectionPath.clear();
    },
    _enableInput: function() {
        cc.eventManager.resumeTarget(this, true);
    },
    _disableInput: function() {
        cc.eventManager.pauseTarget(this, true);
    },
    onMatchUpdated:function (data) {
        if ( MatchStore.hasMatches() ) {
            this._drawSelectionPath();
        } else {
            this._enableInput();
        }
    },
    onMatchScored:function (data) {
        if (ScoringStore.hasScoring()) {
            this._selectionPath.clear();
        }
    },
    onSelectStart: function(event) {
        var col = this.eventCoordinateToColumn(event);
        var row = this.eventCoordinateToRow(event);
        var tile = GameBoardStore.tileForColRow(col, row);

        var ac = new BeginMatchActionCreator(tile);
        ac.create();
    },
    onSelectMove: function(event) {
        var col = this.eventCoordinateToColumn(event);
        var row = this.eventCoordinateToRow(event);
        var tile = GameBoardStore.tileForColRow(col, row);
        var matches = MatchStore.getState().matches;
        var xPos = event.getLocationX();
        var yPos = event.getLocationY();
        var payload = {
            tile: tile,
            matches: matches,
            position: {
                x: xPos,
                y: yPos
            }
        }

        var ac = new BeginMatchActionCreator(payload);
        ac.create();
    },
    onSelectEnd: function(event) {
        this._disableInput();

        var matches = MatchStore.getState().matches;
        var ac = new EndMatchActionCreator(matches);
        ac.create();
    },
    eventCoordinateToColumn:function(event) {
        var xPos = event.getLocationX();
        var yPos = event.getLocationY();
        var pnt = cc.director.convertToGL( cc.p(xPos, yPos) );
        return pnt.x;
    },
    eventCoordinateToRow:function(event) {
        var xPos = event.getLocationX();
        var yPos = event.getLocationY();
        var pnt = cc.director.convertToGL( cc.p(xPos, yPos) );
        return pnt.y;
    }
});
