var ContinueMatchActionCreator = cc.Class.extend({
    create:function () {
        if (!this._hasSelectedTile()) return;

        if (this._isPreviousTile()) {
            var action = new FluxAction("BACKTRACK_TILE_MATCH", null);
            FluxDispatcher.dispatch(action);
        } else {
            var action = new FluxAction("CONTINUE_TILE_MATCH", this.payload.tile);
            FluxDispatcher.dispatch(action);

        }

    },
    _hasSelectedTile: function() {
        return this.payload.tile !== null;
    },
    _isAdjacentTile: function() {

    },
    _isPreviousTile: function() {

    }
})
