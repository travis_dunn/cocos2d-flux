var StartNewGameActionCreator = cc.Class.extend({
    create:function () {
        var isNotTile = this.payload === null;
        if (isNotTile) return;

        var action = new FluxAction("BEGIN_TILE_MATCH", this.payload);
        FluxDispatcher.dispatch(action);
    }
})
