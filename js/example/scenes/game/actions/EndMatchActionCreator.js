var EndMatchActionCreator = cc.Class.extend({
    create:function () {
        var MIN_MATCH_LENGTH = 3;
        if (this.payload.length >= MIN_MATCH_LENGTH) {
            var points = 0;
            var payload = {
                matches: this.getScorings(),
                points: points
            }
            var action = new FluxAction("COMPLETE_TILE_MATCH", payload);
            FluxDispatcher.dispatch(action);
        } else {
            var action = new FluxAction("CANCEL_TILE_MATCH", null);
            FluxDispatcher.dispatch(action);
        }
    },
    getScorings:function() {
        var scorings = [];
        for (var i = 0; i < this.payload.length; i++) {
            var match = this.payload[i];
            var scoring = {
                x: match.row,
                y: match.column
            }
            scorings.push(scoring);
        }
        return scorings;
    }
})
