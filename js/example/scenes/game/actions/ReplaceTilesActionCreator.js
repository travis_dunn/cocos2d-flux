var ReplaceTilesActionCreator = cc.Class.extend({
    create:function () {
        var tiles = Dealer.dealTiles(this.payload)
        var action = new FluxAction("DEAL_REPLACEMENT_TILES", tiles);
        FluxDispatcher.dispatch(action)
    }
})
