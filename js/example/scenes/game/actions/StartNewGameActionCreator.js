var StartNewGameActionCreator = cc.Class.extend({
    create:function () {
        this.payload.tiles = Dealer.getTiles(this.payload.difficultySetting)
        var action = new FluxAction("START_NEW_GAME", this.payload);
        FluxDispatcher.dispatch(action)
    }
})
