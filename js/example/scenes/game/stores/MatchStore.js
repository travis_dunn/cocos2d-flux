var MatchStore = FlexStore.create({
    state: {
        matches: []
    },
    onDispatch:function (action) {
        switch(action.type) {
            case "COMPLETE_TILE_MATCH":

                break;
            default:
                // do nothing
        };

        this._emitChange();
    },
    hasMatches: function() {
        return this.state.matches.length ? true : false;
    }
});
