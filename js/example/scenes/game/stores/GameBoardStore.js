var GameBoardStore = FlexStore.create({
    _state: {
        tiles: []
    },
    ctor:function () {
        this._super();
        this.initTiles();
    },
    initTiles: function() {
        this._state.tiles = [];
        for (var y = 0; y < Config.board.height; y++) {
            this._state.tiles.push([]);
            for (var x = 0; x < Config.board.width; x++) {
                this._state.tiles[this._state.tiles.length - 1].push(null);
            }
        }
    },
    onDispatch: function (action) {
        switch(action.type) {
            case "COMPLETE_TILE_MATCH":
                break;
            case "DEAL_REPLACEMENT_TILES":
                var replacementTiles = action.payload;
                for (var y = 0; y < Config.board.height; y++) {
                    this._state.tiles.push([]);
                    for (var x = 0; x < Config.board.width; x++) {
                        this._state.tiles[y][x] = replacementTiles.shift();
                    }
                }

                this._emitChange();
                break;
            default:
                // do nothing
        };
    },
    tileForColRow: function(col, row) {
        return this._state.tiles[col][row];
    },
    removeTileAtColRow: function(col, row) {
        this._state.tiles[col][row] = null;
    },
    isMissingTiles: function() {
        return tiles.length === 0;
    }
});
