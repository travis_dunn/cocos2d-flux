var DealerStore = FlexStore.create({
    _state: {
        queue: []
    },
    onDispatch: function (action) {
        switch(action.type) {
            case "DEAL_REPLACEMENT_TILES":
                var replacementTiles = action.payload;
                for (var i = 0; i < replacementTiles.length; i++) {
                    this._state.queue.push( replacementTiles[i] );
                }
                this._emitChange();
                break;
            default:
                // do nothing
        };

    }
});
