var ScoringStore = FlexStore.create({
    state: {
        scorings: []
    },
    onDispatch: function (action) {
        switch(action.type) {
            case "COMPLETE_TILE_MATCH":
                this.state.scorings = action.payload;
                this._emitChange();
                break;
            default:
                // do nothing
        };
    },
    hasScoring: function() {
        return this.state.scorings.length ? true : false;
    }
});
