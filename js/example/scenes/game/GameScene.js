var GameScene = cc.Scene.extend({
    _hudLayer: null,
    _boardLayer: null,
    onEnter:function () {
        this._super();

        this._hudLayer = new GameHudViewLayer();
        this._boardLayer = new GameBoardViewLayer();
        this.addChild(this._boardLayer);
        this.addChild(this._hudLayer);

    },
    newGame:function () {
        var difficulty = GameSettingsStore.getState().gameDifficulty;

        var payload = {
            gameDifficulty: difficulty
        }
        var ac = new StartNewGameActionCreator(payload);
        ac.create();
    }
});
