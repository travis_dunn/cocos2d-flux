var GameSettingsViewLayer = FluxView.extend({
    ctor:function () {
        this._super();
        this.init();
    },
    init:function () {
        this.initGameDifficultyWidget();
        this.initButtons();

        GameSettingsStore.addEventListener(this.onSettingsUpdated);
    },
    initGameDifficultyWidget: function() {
        var easyDifficultyCheckBox = new ccui.CheckBox();
        var normalDifficultyCheckBox = new ccui.CheckBox();
        var hardDifficultyCheckBox = new ccui.CheckBox();

        easyDifficultyCheckBox.addEventListener(function(){ this.onChangeSettings(0) }, this);
        normalDifficultyCheckBox.addEventListener(function(){ this.onChangeSettings(1) }, this);
        hardDifficultyCheckBox.addEventListener(function(){ this.onChangeSettings(2) }, this);

        this.addChild(easyDifficultyCheckBox);
        this.addChild(normalDifficultyCheckBox);
        this.addChild(hardDifficultyCheckBox);

        this.gameDifficulties = [easyDifficultyCheckBox, normalDifficultyCheckBox, hardDifficultyCheckBox];
    },
    initButtons: function() {
        var exitButton = new ccui.Button();

        exitButton.addTouchEventListener(this.onSave);
        this.addChild(exitButton);
    },
    onChangeSettings:function (gameDifficulty) {
        var settings = {
            gameDifficulty: gameDifficulty,
            animationSpeed: 50,
            audioVolume: 75
        }
        var ac = new SaveGameSettingsActionCreator(settings);
        ac.create();
    },
    onSave:function () {
        var settings = GameSettingsStore.getState();
        var ac = new SaveGameSettingsActionCreator(settings);
        ac.create();
    },
    onSettingsUpdated:function () {
        var settings = GameSettingsStore.getState();

        for (var i = 0; i < this.gameDifficulties.length; i++) {
            var isSelected = settings.gameDifficulty === i;
            this.gameDifficulties[i].setSelectedState(isSelected)
        }
    }
});
