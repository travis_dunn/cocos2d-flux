var GameSettingsScene = cc.Scene.extend({
    _gameSettingsLayer: null,
    onEnter:function () {
        this._super();

        this._gameSettingsLayer = new GameSettingsViewLayer();
        this.addChild(this._gameSettingsLayer);
    }
});
