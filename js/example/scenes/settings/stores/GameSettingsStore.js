var GameSettingsStore = FlexStore.create({
    state: {
        gameDifficulty: 1,
        animationSpeed: 50,
        audioVolume: 75
    },
    onDispatch:function (action) {
        switch(action.type) {
            default:
                // do nothing
        };

        this._emitChange();
    }
});
