var SaveGameSettingsActionCreator = cc.Class.extend({
    create:function () {
        GameSettingsData.storeSettings(this.payload, function() {
            cc.director.popScene(new MenuScene());
        })
    }
})
