var UpdateGameSettingsActionCreator = cc.Class.extend({
    create:function () {
        var action = new FluxAction("UPDATE_GAME_SETTINGS", this.payload);
        FluxDispatcher.dispatch(action)
    }
})
