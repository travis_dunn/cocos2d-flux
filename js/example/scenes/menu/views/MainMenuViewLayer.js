var MainMenuViewLayer = FluxView.extend({
    ctor:function () {
        this._super();
        this.init();
    },
    init:function () {
        var newGameMenuItem = new cc.MenuItemFont("New Game", this, this.onNewGame);
        var gameSettingsMenuItem = new cc.MenuItemFont("Settings", this, this.onGameSettings);

        var menu = cc.Menu(newGameMenuItem, gameSettingsMenuItem);
        this.addChild(menu);
    },
    onNewGame: function() {
        var ac = new OpenNewGameActionCreator();
        ac.create();
    },
    onGameSettings: function() {
        var ac = new OpenGameSettingsActionCreator();
        ac.create();
    }
});
